import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class LeftPanel extends YoloElement {
  @property({ type: String }) title = '';

  searchHandler = () => {
    // console.log('search');
  };

  showDonate = () => {
    const modal = document.querySelector('#support-modal');
    // console.log(modal);
    modal?.classList.toggle('hidden');

    const modalDialog = document.querySelector('.support-myModal');

    const body = document.querySelector('body');
    modalDialog?.classList.toggle('absolute');
    body?.classList.toggle('fixed');

    // modalDialog?.classList.add('top-3/5');
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };

  render() {
    return html` <div class="col-start-1 col-end-3 lg:block ">
      <p class="ml-4 mt-10 mb-5 text-2xl font-semibold">Categories</p>
      <div id="categoriesList" class="mb-6 overflow-y-scroll ml-2 h-108">
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/politic"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Politic</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/editorial"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Editorial</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/barfe"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Bârfe</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/zvon"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Zvon</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/previziuni-false"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Previziuni False
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/gafe-politice"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Gafe Politice
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/breaking-news"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Breaking News
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="/economic"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Economic</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Social</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Externe</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Sănătate</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Coronavirus
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Sport</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Life</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Comunicate
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Editorialiști
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Horoscop</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Cultură</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Știință</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Tehnologie
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Bussines</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Curs Valutar
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Loto</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Legislație
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Engleză</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Blog</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Vlog</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Foto</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Controverse
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Corupție</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">
            Campanii de presă
          </p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Sondaj</p>
        </a>
        <a
          id="category"
          class="grid grid-cols-5 col-start-1 col-end-1  py-4 px-4 font-semi-bold text-gray-600 hover:bg-blue-700 hover:text-white focus:bg-purple-300 focus:text-purple-700 w-68"
          href="#"
        >
          <svg
            class="fill-current text-white-600 hover:text-white focus:text-purple-800"
            width="25"
            height="23"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              clip-rule="evenodd"
              d="M12.727 19.763l-1.45-1.21c-5.15-4.281-8.55-7.104-8.55-10.57 0-2.823 2.42-5.041 5.5-5.041 1.74 0 3.41.742 4.5 1.916 1.09-1.174 2.76-1.916 4.5-1.916 3.08 0 5.5 2.218 5.5 5.042 0 3.465-3.4 6.288-8.55 10.578l-1.45 1.2z"
            />
          </svg>
          <p id="name" class="col-start-2 col-end-5 font-semibold">Arhivă</p>
        </a>
      </div>
      <a class="mt-6 ml-4 text-gray-500 font-semibold underline" href="#"
        >Privacy policy</a
      >
      <p class=" mt-6 mb-6 ml-4 text-gray-500 text-sm">
        We are struggling to keep this website ad-free
      </p>
      <div class="flex justify-center flex-col mx-4">
        <button
          class=" donate-button mb-3 rounded border border-gray-400 border-opacity-25 lg:w-4/5 w-full  h-10 lg:ml-4 text-purple-700 font-bold tracking-wider"
          type="button"
          @click="${this.showDonate}"
        >
          DONATE
        </button>
        <div id="socialButtons grid grid-cols-3">
          <a
            href="//www.instagram.com/kappa.london/"
            target="_blank"
            class="rounded border border-gray-400 border-opacity-25 lg:mr-0  lg:ml-4 lg:w-1/4 w-full h-10"
            type="button"
          >
            <svg
              class="ml-auto mr-auto mt-2"
              width="19"
              height="20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M13.985 2.63c0-.262.262-.654.523-.654h1.963c.262 0 .523.261.523.654v2.094c0 .261-.261.654-.523.654h-1.963c-.261 0-.523-.262-.523-.654V2.63zM9.667 5.247c1.308 0 2.486.523 3.402 1.44h5.888V4.33c0-2.093-1.57-3.664-3.664-3.664H5.348v4.58h-.785V.667h-.392-.131v4.58h-.785V.667c-.131 0-.393.131-.524.131v4.318h-.785V1.19C1.03 1.845.376 2.892.376 4.2v2.355h5.888c.786-.785 2.094-1.308 3.403-1.308z"
                fill="#6200EE"
              />
              <path
                d="M9.667 12.706c-1.44 0-2.748-1.178-2.748-2.748 0-1.44 1.178-2.748 2.748-2.748 1.44 0 2.748 1.177 2.748 2.748-.131 1.57-1.309 2.748-2.748 2.748zm2.355-5.496c-.261-.262-.523-.393-.916-.524-.392-.261-.916-.392-1.44-.392-.523 0-1.046.13-1.439.392-.392.131-.654.393-.916.524-.785.654-1.177 1.7-1.177 2.748 0 1.963 1.57 3.663 3.664 3.663 1.963 0 3.664-1.7 3.664-3.663-.262-1.047-.655-2.094-1.44-2.748z"
                fill="#6200EE"
              />
              <path
                d="M4.04 19.249h11.123c1.963 0 3.664-1.702 3.664-3.664V7.21h-5.365c.523.785.916 1.701.916 2.748a4.691 4.691 0 01-4.711 4.711 4.691 4.691 0 01-4.71-4.711c0-1.047.26-1.963.915-2.748H.507v8.375c-.13 1.962 1.57 3.664 3.533 3.664z"
                fill="#6200EE"
              />
            </svg>
          </a>
          <a
            href="https://www.facebook.com/kappalondon/"
            target="_blank"
            class="rounded border border-gray-400 border-opacity-25  lg:mr-0  lg:ml-1.5 lg:w-1/4 w-full h-10"
            type="button"
          >
            <svg
              class="ml-auto mr-auto mt-2"
              width="10"
              height="19"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M2.644 18.776H6.44V9.354h2.617l.262-3.14H6.439V4.382c0-.785.131-1.047.916-1.047h1.963V.063H6.701c-2.748 0-4.057 1.178-4.057 3.534v2.486H.682v3.14h1.962v9.553z"
                fill="#6200EE"
              />
            </svg>
          </a>
          <a
            href="https://www.linkedin.com/company/kappalondon"
            target="_blank"
            class="rounded border border-gray-400 border-opacity-25 lg:mr-0 lg:ml-1.5 lg:w-1/4 w-full h-10"
            type="button"
          >
            <svg
              class="ml-auto mr-auto mt-2"
              width="20"
              height="20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19.82 18.332c0 .786-.654 1.309-1.439 1.309H2.286c-.785 0-1.44-.654-1.44-1.309V1.976c0-.786.655-1.309 1.44-1.309h16.095c.785 0 1.44.654 1.44 1.309v16.356z"
                fill="#6200EE"
              />
              <path
                d="M13.67 7.864c-1.57 0-2.225.916-2.617 1.44V8.126h-2.88v8.637h2.88V11.92c0-.262 0-.523.13-.654.131-.524.655-1.047 1.44-1.047 1.047 0 1.44.785 1.44 1.963v4.58h2.878V11.79c0-2.748-1.308-3.926-3.271-3.926zM5.296 3.938c-1.047 0-1.57.655-1.57 1.44 0 .785.654 1.44 1.57 1.44 1.047 0 1.57-.655 1.57-1.44 0-.916-.654-1.44-1.57-1.44zM6.735 8.126H3.856v8.636h2.88V8.127z"
                fill="#fff"
              />
            </svg>
          </a>
        </div>
      </div>
    </div>`;
  }
}
customElements.define('left-panel', LeftPanel);
