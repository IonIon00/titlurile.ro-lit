import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class LeftIcon extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="38"
        height="36"
        viewBox="0 0 38 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="37.5" height="36" rx="4" fill="white" />
        <path
          d="M12.3431 17.4697C12.0503 17.7626 12.0503 18.2374 12.3431 18.5303L17.1161 23.3033C17.409 23.5962 17.8839 23.5962 18.1768 23.3033C18.4697 23.0104 18.4697 22.5355 18.1768 22.2426L13.9341 18L18.1768 13.7574C18.4697 13.4645 18.4697 12.9896 18.1768 12.6967C17.8839 12.4038 17.409 12.4038 17.1161 12.6967L12.3431 17.4697ZM24.6266 17.25L12.8735 17.25L12.8735 18.75L24.6266 18.75L24.6266 17.25Z"
          fill="#6200EE"
        />
        <rect
          x="0.5"
          y="0.5"
          width="36.5"
          height="35"
          rx="3.5"
          stroke="black"
          stroke-opacity="0.12"
        />
      </svg>
    `;
  }
}

customElements.define('left-icon', LeftIcon);
