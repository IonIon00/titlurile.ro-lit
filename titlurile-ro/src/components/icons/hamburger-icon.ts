import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class HamburgerIcon extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="19"
        height="13"
        viewBox="0 0 19 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M0.912109 12.5449H18.9121V10.5449H0.912109V12.5449ZM0.912109 7.54492H18.9121V5.54492H0.912109V7.54492ZM0.912109 0.544922V2.54492H18.9121V0.544922H0.912109Z"
          fill="#4F4F4F"
        />
      </svg>
    `;
  }
}

customElements.define('hamburger-icon', HamburgerIcon);
