import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class FacebookNew extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect
          x="0.803711"
          y="0.435547"
          width="18.5813"
          height="18.5819"
          rx="3.20376"
          fill="#6200EE"
        />
        <path
          d="M8.53051 15.9375H11.0496V9.68309H12.7869L12.9606 7.59842H11.0496V6.38241C11.0496 5.86122 11.1364 5.68724 11.6576 5.68724H12.9606V3.51562H11.2233C9.39913 3.51562 8.53051 4.29757 8.53051 5.86114V7.51148H7.22754V9.59614H8.53051V15.9375Z"
          fill="white"
        />
      </svg>
    `;
  }
}

customElements.define('facebook-new', FacebookNew);
