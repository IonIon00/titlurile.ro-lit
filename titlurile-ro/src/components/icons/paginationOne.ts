import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class PaginationOne extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="38"
        height="36"
        viewBox="0 0 38 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect x="1" y="0.5" width="36.5" height="35" rx="3.5" fill="white" />
        <path
          d="M20.4932 23H18.8389V15.0088L16.3984 15.8428V14.4414L20.2812 13.0127H20.4932V23Z"
          fill="#6200EE"
        />
        <rect
          x="1"
          y="0.5"
          width="36.5"
          height="35"
          rx="3.5"
          stroke="#A5A6F6"
        />
      </svg>
    `;
  }
}

customElements.define('pagination-one', PaginationOne);
