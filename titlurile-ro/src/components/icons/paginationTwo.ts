import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class PaginationTwo extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="38"
        height="36"
        viewBox="0 0 38 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="37.5" height="36" rx="4" fill="white" />
        <path
          d="M22.1875 23H15.4951V21.8584L18.8105 18.2422C19.2891 17.709 19.6286 17.2646 19.8291 16.9092C20.0342 16.5492 20.1367 16.1891 20.1367 15.8291C20.1367 15.3551 20.0023 14.9723 19.7334 14.6807C19.4691 14.389 19.1113 14.2432 18.6602 14.2432C18.1224 14.2432 17.7054 14.4072 17.4092 14.7354C17.113 15.0635 16.9648 15.5124 16.9648 16.082H15.3037C15.3037 15.4759 15.4404 14.9313 15.7139 14.4482C15.9919 13.9606 16.3861 13.5824 16.8965 13.3135C17.4115 13.0446 18.0039 12.9102 18.6738 12.9102C19.64 12.9102 20.401 13.154 20.957 13.6416C21.5176 14.1247 21.7979 14.7946 21.7979 15.6514C21.7979 16.1481 21.6566 16.6699 21.374 17.2168C21.096 17.7591 20.638 18.3766 20 19.0693L17.5664 21.6738H22.1875V23Z"
          fill="#6200EE"
        />
        <rect
          x="0.5"
          y="0.5"
          width="36.5"
          height="35"
          rx="3.5"
          stroke="black"
          stroke-opacity="0.12"
        />
      </svg>
    `;
  }
}

customElements.define('pagination-two', PaginationTwo);
