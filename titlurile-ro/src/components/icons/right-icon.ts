import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class RightIcon extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <svg
        width="38"
        height="36"
        viewBox="0 0 38 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect x="0.5" width="37.5" height="36" rx="4" fill="white" />
        <path
          d="M25.6569 17.4697C25.9498 17.7626 25.9498 18.2374 25.6569 18.5303L20.8839 23.3033C20.591 23.5962 20.1161 23.5962 19.8233 23.3033C19.5304 23.0104 19.5304 22.5355 19.8233 22.2426L24.0659 18L19.8233 13.7574C19.5304 13.4645 19.5304 12.9896 19.8233 12.6967C20.1161 12.4038 20.591 12.4038 20.8839 12.6967L25.6569 17.4697ZM13.3734 17.25L25.1266 17.25L25.1266 18.75L13.3734 18.75L13.3734 17.25Z"
          fill="#6200EE"
        />
        <rect
          x="1"
          y="0.5"
          width="36.5"
          height="35"
          rx="3.5"
          stroke="black"
          stroke-opacity="0.12"
        />
      </svg>
    `;
  }
}

customElements.define('right-icon', RightIcon);
