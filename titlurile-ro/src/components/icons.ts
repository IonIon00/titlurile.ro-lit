import { html } from 'lit-element';

import { YoloElement } from '../YoloElement.js';

export class HeadlineIcon extends YoloElement {
  path: string;

  static get properties() {
    return {
      path: { type: String },
    };
  }

  constructor() {
    super();
    this.path = '';
  }

  render() {
    return html`
      <img
        src=${this.path}
        class="object-contain w-10 h-10 lg:w-10 lg:h-10"
        alt="company1"
      />
    `;
  }
}

customElements.define('headline-icon', HeadlineIcon);
