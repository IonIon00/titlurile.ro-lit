import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class Header extends YoloElement {
  @property({ type: String }) title = 'Titlurile.ro';

  render() {
    return html` <div
      class="border rounded-3xl border-gray-400 border-opacity-50 mb-5 mx-10 lg:mx-0"
    >
      <div class="flex flex-row justify-between p-6">
        <div class="mb-8 mt-2 flex flex-col justify-start">
          <h2
            class="text-4xl lg:text-2xl font-semibold   "
            onclick="showModal()"
          >
            Lytton, B.C. breaks Canada’s all-time heat record for third time
          </h2>
          <p class="text-gray-400 text-2xl lg:text-sm">
            Our agency name - 19 hours ago
          </p>
          <div id="newsCompanies" class="flex flex-row mt-2 gap-2">
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company1.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="icons"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company2.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="icons"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company3.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="img"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company4.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="img"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company2.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="icons"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company4.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="img"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company3.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="img"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
            <div class="border rounded-xl px-2 py-2">
              <img
                src="./assets/company4.jpg"
                class="w-20 h-20 lg:w-10 lg:h-10"
                alt="icons"
              />
              <p class="mt-1 text-gray-400 lg:text-xs text-xl">5h ago</p>
            </div>
          </div>
          <button
            class="text-left mt-6 text-purple-700 font-bold tracking-wide news-button text-3xl lg:text-lg"
            onclick="showModal()"
          >
            VIEW FULL COVERAGE
          </button>
        </div>
        <div class="flex flex-col justify-between">
          <img
            class="object-cover w-24 h-24"
            src="./assets/image.jpg"
            alt="img"
          />
          <svg
            class="ml-16 mb-12"
            width="16"
            height="9"
            viewBox="0 0 16 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1.41113 1.27938L7.80326 7.67151L14.1955 1.2793"
              stroke="black"
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        </div>
      </div>
    </div>`;
  }
}
customElements.define('header-component', Header);
