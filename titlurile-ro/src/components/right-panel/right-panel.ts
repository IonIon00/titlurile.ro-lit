import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

import './weather-widget.js';

export class RightPanel extends YoloElement {
  @property({ type: String }) title = '';

  @property({ type: Object }) todayWeather = {};

  @property({ type: Array }) weather: any = [];

  @property({ type: Boolean }) isLoading = true;

  @property({ type: Number }) KELVIN = 273;

  name: string;

  props: any;

  constructor() {
    super();
    this.name = '';
    this.weather = [];
    this.KELVIN = 273;
    this.isLoading = true;
  }

  fetchWeather = async () => {
    const API_KEY = '7aa4fa3afa4de71c41d10d92669a6ac1';

    const baseURL = `https://api.openweathermap.org/data/2.5/onecall?lat=44.439663&lon=26.096306&exclude=hourly&appid=${API_KEY}`;
    const response = await fetch(baseURL);
    const data = await response.json();

    const { current, timezone } = data;

    this.weather.push(current);

    this.name = timezone;

    console.log(data.data);

    this.isLoading = false;
  };

  connectedCallback() {
    super.connectedCallback();
    this.fetchWeather();
  }

  render() {
    const result: any[] = this.weather;

    console.log(result);

    return html`

    <div class=" mt-12 col-start-10 col-end-12 hidden lg:block" id="rightSide">
    <weather-widget
      .city="${this.name}"
      .description="${result[0].weather[0].description}"
      .icon="${result[0].weather[0].icon}"
      .temperature="${result[0].temp}"
      .isCelsiusActive="${this.props}"
      .isKelvinActive="${this.props}"
      .isFahrenheitActive="${this.props}"
      ></weather-widget>



    <div id="tags" class="bg-gray-50 mt-10 border-2 border-gray-400 border-opacity-25 rounded-3xl w-11/12"><!--divide-y divide-gray-500-->
        <div class="mr-6 divide-y divide-gray-400">
            <p class="text-2xl ml-6 mt-6 mb-6 mr-6">Tags</p>
            <div id="tagButtons">
                <button class="bg-white ml-2 mt-6 mb-2 h-10 w-20 rounded-3xl border border-gray-400 border-opacity-25">Vaccine</button>
                <button class="bg-white ml-2 mr-2 mb-2 h-10 w-20 rounded-3xl border border-gray-400 border-opacity-25">Covid-19</button>
                <button class="bg-white ml-2 mr-2 mb-2 h-10 w-16 rounded-3xl border border-gray-400 border-opacity-25">News</button>
                <button class="bg-white ml-2 mr-2 mb-2 h-10 w-20 rounded-3xl border border-gray-400 border-opacity-25">Police</button>
                <button class="bg-white ml-2 mr-2 mb-2 h-10 w-20 rounded-3xl border border-gray-400 border-opacity-25">Design</button>
                <button class="bg-white ml-2 mb-6 mr-2 h-10 w-24 rounded-3xl border border-gray-400 border-opacity-25">Marketing</button>
            </div>
        </div>
    </div>

            <form class="my-12"><!--action="/action_page.php"-->
                <label>Subscribe to newsmail</label>

                <input required id="email" type="email" name="email" class="peer w-11/12 px-2 h-12 border resize-none" placeholder="Email address" ></input>
                <input type="submit" value="SUBSCRIBE" class="bg-white mt-4 rounded border border-gray-400 border-opacity-25 h-8 w-11/12 text-purple-700 font-bold tracking-wider"></input>
            </form>
    </div>
`;
  }
}
customElements.define('right-panel', RightPanel);
