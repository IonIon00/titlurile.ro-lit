import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

import '../icons/cloud-day-icon.js';

export class DayWeather extends YoloElement {
  day: string;

  icon: string;

  temperatureMax: number;

  temperatureMin: number;

  isCelsius: boolean;

  isKelvin: boolean;

  isFahrenheit: boolean;

  constructor() {
    super();
    this.day = '';
    this.temperatureMax = 99;
    this.icon = '';
    this.temperatureMin = 0;
    this.isCelsius = false;
    this.isKelvin = false;
    this.isFahrenheit = false;
  }

  render() {
    return html`
      <div class="flex flex-col">
        <h4 class="text-sm text-bold font-roboto">${this.day}</h4>

        <cloud-day-icon class="my-3 object-fit"></cloud-day-icon>
        ${this.isCelsius === true
          ? html`<h3 class="text-xs font-roboto">
                ${Math.floor(this.temperatureMax - 273)}°C
              </h3>
              <h3 class="text-xs text-gray-300 font-roboto">
                ${Math.floor(this.temperatureMin - 273)}°C
              </h3>`
          : ''}
        ${this.isKelvin === true
          ? html`<h3 class="text-xs font-roboto">
                ${Math.floor(this.temperatureMax)}°K
              </h3>
              <h3 class="text-xs text-gray-300 font-roboto">
                ${Math.floor(this.temperatureMin)}°K
              </h3>`
          : ''}
        ${this.isFahrenheit === true
          ? html`<h3 class="text-xs font-roboto">
                ${Math.floor(this.temperatureMax * (9 / 5) - 459.67)}°F
              </h3>
              <h3 class="text-xs text-gray-300 font-roboto">
                ${Math.floor(this.temperatureMin * (9 / 5) - 459.67)}°F
              </h3>`
          : ''}
      </div>
    `;
  }
}

customElements.define('day-weather', DayWeather);
