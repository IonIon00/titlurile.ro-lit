import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

import './day-weather.js';

import '../icons/cloud-icon.js';

export class WeatherWidget extends YoloElement {
  city: string;

  description: string;

  icon: string;

  temperature: number;

  weekDays: string[];

  isCelsiusActive: boolean;

  isKelvinActive: boolean;

  isFahrenheitActive: boolean;

  constructor() {
    super();
    this.city = '';
    this.description = '';
    this.icon = '';
    this.temperature = 0;
    this.days = [];
    this.weekDays = ['Today', 'Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
    this.isCelsiusActive = false;
    this.isKelvinActive = false;
    this.isFahrenheitActive = false;
  }

  @property({ type: Array }) days: any = [];

  fetchForcast = async () => {
    const API_KEY = '3a1be8ff367f811e3920804425e1fae3';

    const baseURL = `https://api.openweathermap.org/data/2.5/onecall?lat=44.439663&lon=26.096306&exclude=hourly&appid=${API_KEY}`;
    const response = await fetch(baseURL);
    const data = await response.json();

    const { daily } = data;

    this.days = daily;

    console.log(data);
  };

  connectedCallback() {
    super.connectedCallback();
    this.fetchForcast();
  }

  static get properties() {
    return {
      days: { type: Array },
    };
  }

  toCelcius = () => {
    this.isCelsiusActive = true;
    console.log(this.isCelsiusActive);
  };

  toKelvin = () => {
    this.isKelvinActive = true;
    console.log(this.isKelvinActive);
  };

  toFahrenheit = () => {
    this.isFahrenheitActive = true;
    console.log(this.isFahrenheitActive);
  };

  render() {
    console.log(this.days);

    return html`
      <div
        id="tags"
        class="bg-white mt-10 border-2 border-gray-400 border-opacity-25 rounded-2xl w-11/12"
      >
        <!--divide-y divide-gray-500-->
        <div class="mx-8 divide-gray-400 h-auto max-w-full py-4 px-0">
          <p
            class="lg:text-2xl text-lg font-semibold  h-auto m-4 ml-0 mb-2 w-full font-roboto"
          >
            Bucharest
          </p>
          <div class="flex flex-row justify-between border-t-2 ">
            <div class="flex flex-col mt-6">
              <p class="text-xs font-roboto text-news-gray font-medium">
                ${this.description}
              </p>
              <h3 class="text-2xl text-news-gray">
                ${Math.floor(this.temperature - 273)}°C
              </h3>
            </div>

            <cloud-icon class="h-16 w-16 mt-6 object-fit"></cloud-icon>
          </div>
          <div class="flex flex-row mt-3 overflow-x-auto  gap-8">
            ${!this.isCelsiusActive &&
            !this.isKelvinActive &&
            !this.isFahrenheitActive
              ? html` ${this.days.map(
                  (day: any, index: any) =>
                    html`<day-weather
                      .day="${this.weekDays[index]}"
                      .icon="${day.weather[0].icon}"
                      .temperatureMax="${day.temp.max}"
                      .temperatureMin="${day.temp.min}"
                      key=${index}
                      .isCelsius="${true}"
                      .isFahrenheit="${false}"
                      .isKelvin="${false}"
                    ></day-weather>`
                )}`
              : html``}
            ${this.isCelsiusActive === true
              ? html`${this.days.map(
                  (day: any, index: any) =>
                    html`<day-weather
                      .day="${this.weekDays[index]}"
                      .icon="${day.weather[0].icon}"
                      .temperatureMax="${day.temp.max}"
                      .temperatureMin="${day.temp.min}"
                      key=${index}
                      .isCelsius="${true}"
                      .isFahrenheit="${false}"
                      .isKelvin="${false}"
                    ></day-weather>`
                )}`
              : html``}
            ${this.isFahrenheitActive === true
              ? html`${this.days.map(
                  (day: any, index: any) =>
                    html`<day-weather
                      .day="${this.weekDays[index]}"
                      .icon="${day.weather[0].icon}"
                      .temperatureMax="${day.temp.max}"
                      .temperatureMin="${day.temp.min}"
                      key=${index}
                      .isCelsius="${false}"
                      .isFahrenheit="${true}"
                      .isKelvin="${false}"
                    ></day-weather>`
                )}`
              : html``}
            ${this.isKelvinActive === true
              ? html`${this.days.map(
                  (day: any, index: any) =>
                    html`<day-weather
                      .day="${this.weekDays[index]}"
                      .icon="${day.weather[0].icon}"
                      .temperatureMax="${day.temp.max}"
                      .temperatureMin="${day.temp.min}"
                      key=${index}
                      .isCelsius="${false}"
                      .isFahrenheit="${false}"
                      .isKelvin="${true}"
                    ></day-weather>`
                )}`
              : html``}
          </div>
          <div class="flex flex-row justify-between my-4">
            <ul class="list-none   flex flex-row  justify-between gap-2">
              <button @click="${this.toCelcius}">
                <li class="text-xs cursor-pointer">C</li>
              </button>
              <li class="text-xs">|</li>
              <button @click="${this.toFahrenheit}">
                <li class="text-xs cursor-pointer">F</li>
              </button>
              <li class="text-xs">|</li>
              <button @click="${this.toKelvin}">
                <li class="text-xs cursor-pointer">K</li>
              </button>
            </ul>
            <p class="text-xs">More on weather.com</p>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('weather-widget', WeatherWidget);
