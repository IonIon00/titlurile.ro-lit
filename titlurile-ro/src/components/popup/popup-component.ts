import { Router } from '@vaadin/router';
import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

// import '../../icons.js';
// import './shareModal.js';
// import '../../icons/shareButton.js';

import '../icons/facebook-new.js';
import '../icons/instagram-new.js';
import '../icons/copy-new.js';
import '../icons/linkedin-new.js';

export class PopupComponent extends YoloElement {
  image: string;

  description: string;

  index: number;

  URL: string;

  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
      source: { type: String },
      description: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.source = '';
    this.time = '';
    this.image = '';
    this.description = '';
    this.index = 0;
    this.URL = '';
  }

  connectedCallback() {
    super.connectedCallback();
  }

  @property({ type: String }) title: string;

  @property({ type: String }) source: string;

  @property({ type: String }) time: string;

  closeModal = () => {
    const modal = this.querySelector('#modal');
    modal?.classList.add('hidden');
    const bodyElement = document.getElementsByTagName('BODY')[0];
    bodyElement.classList.remove('fixed');
    // window.pageYOffset=100;
    modal?.classList.add('fadeOut');
    modal?.classList.remove('fadeIn');
    const router = new Router(this);

    console.log(router.location.baseUrl);

    // router.setRoutes([
    //   { path: router.location.baseUrl, component: 'titlurile-ro' },
    // ]);
    window.location.pathname = router.location.baseUrl;
  };

  showModal = () => {
    const modal = this.querySelector('#share-modal');
    modal?.classList.remove('hidden');

    const modalDialog = this.querySelector('.share-myModal');
    const body = document.querySelector('body');

    body?.classList.add('fixed');
    modalDialog?.classList.add('top-3/5');
    document.body.scrollTop = window.screenY;
    document.documentElement.scrollTop = window.screenY;
  };

  copyURL = () => {
    console.log('ceva');
  };

  getToInstagram = () => {
    window.location.href = '//www.instagram.com/kappa.london/';
  };

  getToFacebook = () => {
    window.location.href = 'https://www.facebook.com/kappalondon/';
  };

  getToLinkedin = () => {
    window.location.href = 'https://www.linkedin.com/company/kappalondon';
  };

  render() {
    return html`
      <div
        id="popup-modal"
        class="mt-20 lg:mt-0 absolute inset-0 bg-black bg-opacity-25  h-screen w-screen hidden  flex  justify-center items-center animated fadeIn faster"
      >
        <div
          class="popup-myModal  lg:mt-0 animate-fadeInUp shadow-inner bg-white flex flex-col relative top-20   right-10  mx-auto rounded-xl p-8 w-full lg:w-1/2"
        >
          <button @click="${this.closeModal}" class="self-end mb-4 text-lg">
            ✕
          </button>

          <div class="grid grid-cols-12 lg:gap-5">
            <div class="col-start-1 col-span-12 lg:col-span-10">
              <h2 class="lg:text-3xl text-lg font-semibold">${this.title}</h2>
              <p class="text-gray-400 lg:text-lg text-xs  mt-2">
                ${this.source} - ${this.time} hours ago
              </p>
              <div class="rounded-lg p-3 border mt-2 ">
                <div
                  class="bg-green-100 inline rounded-lg px-2 py-1 text-md lg:text-xl "
                >
                  Editor's quote
                </div>
                <p class="text-md lg:text-lg font-bold mt-4">
                  by ${this.source}
                </p>
                <p
                  class="text-gray-500 leading-none max-w-2xl text-md lg:text-sm mt-1"
                >
                  ${this.description}
                </p>
              </div>
            </div>
            <div class="col-span-4 col-start-11">
              <img
                src=${this.image}
                class="hidden lg:block object-cover w-full h-1/2 "
                alt="headline-modal"
              />
            </div>
            <img
              src=${this.image}
              class="my-4 col-span-12 lg:hidden object-cover w-full h-48"
              alt="headline-modal"
            />
          </div>
          <div class="flex flex-row mt-6 gap-3 justify-between">
            <div>
              <button
                class="bg-white text-md lg:text-sm h-10 w-30 px-3 lg:w-20 rounded-3xl border border-gray-400 border-opacity-25"
              >
                Vaccine
              </button>
              <button
                class="bg-white text-md lg:text-sm  h-10 w-30 px-3 lg:w-30 rounded-3xl border border-gray-400 border-opacity-25"
              >
                Covid-19
              </button>
              <button
                class="bg-white text-md lg:text-sm  h-10 w-30 px-3 lg:w-20 rounded-3xl border border-gray-400 border-opacity-25"
              >
                News
              </button>
            </div>
            <div class="flex flex-row gap-4">
              <a href="/news/${this.index}" class="mt-3">
                <button @click="${this.copyURL}">
                  <copy-new></copy-new>
                </button>
              </a>
              <button @click="${this.getToInstagram}">
                <instagram-new></instagram-new>
              </button>
              <button @click="${this.getToFacebook}">
                <facebook-new></facebook-new>
              </button>
              <button @click="${this.getToLinkedin}">
                <linkedin-new></linkedin-new>
              </button>
            </div>
          </div>
          <div class="mb-8 mt-2 flex flex-col  h-64 overflow-y-scroll ">
            <div
              class="mt-3 flex px-3 w-full flex-row rounded-lg  bg-gray-100 py-1 h-full"
            >
              <div class="flex flex-col">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p
                  class="mt-1 text-gray-400 lg:text-xs text-lg whitespace-nowrap"
                >
                  ${this.time}h ago
                </p>
              </div>
              <div
                class="rounded-r-xl flex flex-row justify-between  w-full h-full pl-5 pr-0"
              >
                <h2
                  class="text-gray-900 font-semibold lg:text-base text-md self-center"
                >
                  Lytton, B.C. break's Canada's all time heat record for the
                  third time
                </h2>
                <svg
                  class="mt-6 ml-5 2xl:ml-12"
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.3675 5.92709L12.7211 6.28064L13.0746 5.92709L12.7211 5.57354L12.3675 5.92709ZM7.88206 0.734521C7.6868 0.539259 7.37021 0.539259 7.17495 0.734521C6.97969 0.929783 6.97969 1.24637 7.17495 1.44163L7.88206 0.734521ZM7.03298 10.5545C6.83772 10.7498 6.83772 11.0664 7.03298 11.2616C7.22824 11.4569 7.54483 11.4569 7.74009 11.2616L7.03298 10.5545ZM0.990094 6.40646L12.3666 6.42709L12.3684 5.42709L0.991908 5.40646L0.990094 6.40646ZM12.7211 5.57354L7.88206 0.734521L7.17495 1.44163L12.014 6.28064L12.7211 5.57354ZM12.014 5.57354L7.03298 10.5545L7.74009 11.2616L12.7211 6.28064L12.014 5.57354Z"
                    fill="#6200EE"
                  />
                </svg>
              </div>
            </div>
            <div
              class="mt-3 flex px-3 w-full flex-row rounded-lg bg-gray-100 py-1 h-full"
            >
              <div class="flex flex-col">
                <headline-icon
                  .path=${'https://i1.sndcdn.com/avatars-000261700769-0uhhy6-t500x500.jpg'}
                ></headline-icon>
                <p
                  class="mt-1 text-gray-400 lg:text-xs text-lg whitespace-nowrap"
                >
                  ${this.time}h ago
                </p>
              </div>
              <div
                class="rounded-r-xl flex flex-row justify-between  w-full h-full pl-5 pr-0"
              >
                <h2
                  class="text-gray-900 font-semibold lg:text-base text-md self-center"
                >
                  Lytton, B.C. break's Canada's all time heat record for the
                  third time
                </h2>
                <svg
                  class="mt-6 ml-5 2xl:ml-12"
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.3675 5.92709L12.7211 6.28064L13.0746 5.92709L12.7211 5.57354L12.3675 5.92709ZM7.88206 0.734521C7.6868 0.539259 7.37021 0.539259 7.17495 0.734521C6.97969 0.929783 6.97969 1.24637 7.17495 1.44163L7.88206 0.734521ZM7.03298 10.5545C6.83772 10.7498 6.83772 11.0664 7.03298 11.2616C7.22824 11.4569 7.54483 11.4569 7.74009 11.2616L7.03298 10.5545ZM0.990094 6.40646L12.3666 6.42709L12.3684 5.42709L0.991908 5.40646L0.990094 6.40646ZM12.7211 5.57354L7.88206 0.734521L7.17495 1.44163L12.014 6.28064L12.7211 5.57354ZM12.014 5.57354L7.03298 10.5545L7.74009 11.2616L12.7211 6.28064L12.014 5.57354Z"
                    fill="#6200EE"
                  />
                </svg>
              </div>
            </div>
            <div
              class="mt-3 flex px-3 w-full flex-row rounded-lg bg-gray-100 py-1 h-full"
            >
              <div class="flex flex-col">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p
                  class="mt-1 text-gray-400 lg:text-xs text-lg whitespace-nowrap"
                >
                  ${this.time}h ago
                </p>
              </div>
              <div
                class="rounded-r-xl flex flex-row justify-between  w-full h-full pl-5 pr-0"
              >
                <h2
                  class="text-gray-900 font-semibold lg:text-base text-md self-center"
                >
                  Lytton, B.C. break's Canada's all time heat record for the
                  third time
                </h2>
                <svg
                  class="mt-6 ml-5 2xl:ml-12"
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.3675 5.92709L12.7211 6.28064L13.0746 5.92709L12.7211 5.57354L12.3675 5.92709ZM7.88206 0.734521C7.6868 0.539259 7.37021 0.539259 7.17495 0.734521C6.97969 0.929783 6.97969 1.24637 7.17495 1.44163L7.88206 0.734521ZM7.03298 10.5545C6.83772 10.7498 6.83772 11.0664 7.03298 11.2616C7.22824 11.4569 7.54483 11.4569 7.74009 11.2616L7.03298 10.5545ZM0.990094 6.40646L12.3666 6.42709L12.3684 5.42709L0.991908 5.40646L0.990094 6.40646ZM12.7211 5.57354L7.88206 0.734521L7.17495 1.44163L12.014 6.28064L12.7211 5.57354ZM12.014 5.57354L7.03298 10.5545L7.74009 11.2616L12.7211 6.28064L12.014 5.57354Z"
                    fill="#6200EE"
                  />
                </svg>
              </div>
            </div>
            <div
              class="mt-3 flex px-3 w-full flex-row rounded-lg bg-gray-100 py-1 h-full"
            >
              <div class="flex flex-col">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/6/60/Curierul_Național.png'}
                ></headline-icon>
                <p
                  class="mt-1 text-gray-400 lg:text-xs text-lg whitespace-nowrap"
                >
                  ${this.time}h ago
                </p>
              </div>
              <div
                class="rounded-r-xl flex flex-row justify-between  w-full h-full pl-5 pr-0"
              >
                <h2
                  class="text-gray-900 font-semibold lg:text-base text-md self-center"
                >
                  Lytton, B.C. break's Canada's all time heat record for the
                  third time
                </h2>
                <svg
                  class="mt-6 ml-5 2xl:ml-12"
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.3675 5.92709L12.7211 6.28064L13.0746 5.92709L12.7211 5.57354L12.3675 5.92709ZM7.88206 0.734521C7.6868 0.539259 7.37021 0.539259 7.17495 0.734521C6.97969 0.929783 6.97969 1.24637 7.17495 1.44163L7.88206 0.734521ZM7.03298 10.5545C6.83772 10.7498 6.83772 11.0664 7.03298 11.2616C7.22824 11.4569 7.54483 11.4569 7.74009 11.2616L7.03298 10.5545ZM0.990094 6.40646L12.3666 6.42709L12.3684 5.42709L0.991908 5.40646L0.990094 6.40646ZM12.7211 5.57354L7.88206 0.734521L7.17495 1.44163L12.014 6.28064L12.7211 5.57354ZM12.014 5.57354L7.03298 10.5545L7.74009 11.2616L12.7211 6.28064L12.014 5.57354Z"
                    fill="#6200EE"
                  />
                </svg>
              </div>
            </div>
            <div
              class="mt-3 flex px-3 w-full flex-row rounded-lg bg-gray-100 py-1 h-full"
            >
              <div class="flex flex-col">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p
                  class="mt-1 text-gray-400 lg:text-xs text-lg whitespace-nowrap"
                >
                  ${this.time}h ago
                </p>
              </div>
              <div
                class="rounded-r-xl flex flex-row justify-between  w-full h-full pl-5 pr-0"
              >
                <h2
                  class="text-gray-900 font-semibold lg:text-base text-md self-center"
                >
                  Lytton, B.C. break's Canada's all time heat record for the
                  third time
                </h2>
                <svg
                  class="mt-6 ml-5 2xl:ml-12"
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M12.3675 5.92709L12.7211 6.28064L13.0746 5.92709L12.7211 5.57354L12.3675 5.92709ZM7.88206 0.734521C7.6868 0.539259 7.37021 0.539259 7.17495 0.734521C6.97969 0.929783 6.97969 1.24637 7.17495 1.44163L7.88206 0.734521ZM7.03298 10.5545C6.83772 10.7498 6.83772 11.0664 7.03298 11.2616C7.22824 11.4569 7.54483 11.4569 7.74009 11.2616L7.03298 10.5545ZM0.990094 6.40646L12.3666 6.42709L12.3684 5.42709L0.991908 5.40646L0.990094 6.40646ZM12.7211 5.57354L7.88206 0.734521L7.17495 1.44163L12.014 6.28064L12.7211 5.57354ZM12.014 5.57354L7.03298 10.5545L7.74009 11.2616L12.7211 6.28064L12.014 5.57354Z"
                    fill="#6200EE"
                  />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
      <share-modal .title="${this.title}"></share-modal>
    `;
  }
}

customElements.define('popup-component', PopupComponent);
