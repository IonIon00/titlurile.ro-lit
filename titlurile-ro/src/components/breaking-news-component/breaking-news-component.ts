import { html, property } from 'lit-element';

import 'lit-pagination';

import { YoloElement } from '../../YoloElement.js';
import './main-news.js';
import './breaking-news-card.js';
import '../icons/left-icon.js';
import '../icons/right-icon.js';

export class BreakingNews extends YoloElement {
  isLoading: boolean;

  perPage: Number;

  page: any;

  rows: Number;

  listArticles: any;

  paginationElement: any;

  static get properties() {
    return {
      news: { type: Array },
    };
  }

  constructor() {
    super();
    this.news = [];
    this.isLoading = true;
    this.perPage = 3;
    this.page = 1;
    this.rows = 4;
  }

  @property({ type: Array }) news = [];

  fetchNews = async () => {
    try {
      // const API_KEY = 'ae34a4b3475c4ad185afad31cdc4a4cf';

      // const baseURL = `https://newsapi.org/v2/top-headlines?country=ro&apiKey=${API_KEY}`;
      const baseURL = `http://localhost:3000/articles`;
      const response = await fetch(baseURL);
      const news = await response.json();

      // this.news = news.articles;
      this.news = news;
      this.isLoading = false;
    } catch (e) {
      console.log(e);
    }
  };

  connectedCallback() {
    super.connectedCallback();
    this.fetchNews();
  }

  firstUpdated() {
    this.fetchNews();
  }

  nextPage = (news: number) => {
    this.fetchNews();

    this.page += 1;
    const next = document.querySelector('.next-button');
    const prev = document.querySelector('.prev-button');

    if (this.page >= Math.ceil(news / 2)) {
      next?.classList.add('hidden');
    } else {
      prev?.classList.remove('hidden');
    }
  };

  prevPage = () => {
    this.fetchNews();

    this.page -= 1;

    const prev = document.querySelector('.prev-button');
    const next = document.querySelector('.next-button');
    if (this.page <= 0) {
      prev?.classList.add('hidden');
    } else {
      next?.classList.remove('hidden');
    }
  };

  paginator = (items: any, page: any, perPage: any) => {
    const currentPage = page || 1;
    const perCurrentPage = perPage || 10;
    const offset = (page - 1) * perPage;
    const paginatedItems = items.slice(offset).slice(0, perPage);
    const totalPages = Math.ceil(items.length / perPage);
    return {
      page: currentPage,
      perPage: perCurrentPage,
      prePage: page - 1 ? page - 1 : null,
      nextPage: totalPages > page ? page + 1 : null,
      total: items.length,
      totalPages,
      data: paginatedItems,
    };
  };

  timeDiffCalc = (dateNow: Date, datePast: Date) => {
    const dateNowMiliseconds = dateNow.getTime();

    const datePastMiliseconds = datePast.getTime();

    const difference = Math.abs(dateNowMiliseconds - datePastMiliseconds);

    const hours = Math.floor((difference / (1000 * 60 * 60)) % 24);

    return hours;
  };

  render() {
    const result: any[] = this.news;

    const filter = result.filter(
      time =>
        this.timeDiffCalc(
          new Date(new Date().toISOString()),
          new Date(time.publishedAt)
        ) < 4
    );

    console.log(filter);

    const filteredNews: any[] = this.paginator(filter, this.page, 2).data;
    return html`<div
      id="breakingNewsCard"
      class="mt-2 bg-red-50 border border-gray-400 border-opacity-50 mb-5 lg:mx-0 p-1 lg:p-4"
    >
      <p class="text-lg lg:text-2xl font-semibold mt-6 ml-5">
        🔥 Breaking news
      </p>
      ${this.isLoading
        ? html` <div class="loader">
            <div class="lds-dual-ring"></div>
            <h2 class="text-white m-6">Se incarca produsele...</h2>
          </div>`
        : html`<div class="mt-6">
            <main-news
              .title="${result[0].title}"
              .time="${this.timeDiffCalc(
                new Date(new Date().toISOString()),
                new Date(result[0].publishedAt)
              ).toString()}"
              .image="${result[0].urlToImage}"
            ></main-news>

            <div class="breaking-news-list">
              ${filteredNews.map(
                (articles, index) =>
                  html`
                    <a href="${articles.url}">
                      <breaking-news-card
                        .title="${articles.title}"
                        .time="${articles.published_at}"
                        .image="${articles.urlToImage}"
                        class="breaking-news-card"
                        key=${index}
                      ></breaking-news-card>
                    </a>
                  `
              )}
            </div>
            <div class="${filter.length <= 2 ? 'hidden' : ''}">
              <div
                class="flex flex-row ${this.page ===
                Math.ceil(filter.length / 2)
                  ? 'flex-start'
                  : 'justify-around '} my-6"
              >
                <button
                  class="prev-button flex mx-auto "
                  @click="${this.prevPage}"
                >
                  <left-icon></left-icon>
                </button>

                <button
                  class="next-button flex mx-auto "
                  @click="${() => this.nextPage(filter.length)}"
                >
                  <right-icon></right-icon>
                </button>
              </div>
            </div>
          </div>`}
    </div> `;
  }
}

customElements.define('breaking-news', BreakingNews);
