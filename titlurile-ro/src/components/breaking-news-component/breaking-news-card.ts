import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class BreakingNewsCard extends YoloElement {
  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.image = '';
  }

  @property({ type: String }) title: string;

  @property({ type: String }) image: string;

  render() {
    return html`
      <div class="mt-4 flex flex-col">
        <div class="flex px-6 h-full xl:h-32 w-full flex-row">
          <img
            class="rounded-l-xl object-cover h-auto w-1/4 lg:w-56"
            src="${this.image}"
            alt="background"
          />
          <div
            class="rounded-r-xl flex flex-row justify-between bg-white w-full h-auto px-5"
          >
            <h2
              class="text-gray-900 font-bold text-sm md:text-xl lg:text-lg self-center py-3 2xl:text-2xl "
            >
              ${this.title}
            </h2>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('breaking-news-card', BreakingNewsCard);
