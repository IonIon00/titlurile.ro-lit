import { html } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

export class MainNewsCard extends YoloElement {
  image: string;

  title: string;

  time: string;

  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.time = '';
    this.image = '';
  }

  render() {
    return html`<div
      class="flex px-6 h-full  w-full flex-col lg:flex-row lg:rounded-l-2xl lg:rounded-r-2xl"
    >
      <img
        class="rounded-t-xl lg:rounded-l-xl object-contain lg:object-cover h-auto w-full lg:w-56"
        src="${this.image}"
        alt="background"
      />
      <div class="lg:rounded-r-xl bg-white w-full h-auto px-7">
        <h2
          class="max-w-[500px]  my-4 lg:mt-10 lg:mb-2  text-gray-900 font-bold text-md md:text-xl 2xl:text-2xl self-center"
        >
          ${this.title}
        </h2>
        <p class="text-gray-400 text-md my-5 lg:mt-0 lg:text-sm xl:text-xl">
          ${this.time} h ago
        </p>
      </div>
    </div>`;
  }
}

customElements.define('main-news', MainNewsCard);
