import { html } from 'lit-element';

import { YoloElement } from '../../../YoloElement.js';

import '../../icons/paypal-icon.js';
import '../../icons/revolut-icon.js';
import '../../icons/patreon-icon.js';

export class SupportModal extends YoloElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

  closeModal = () => {
    const modal = this.querySelector('#support-modal');
    modal?.classList.add('hidden');
    const bodyElement = document.querySelector('body');
    bodyElement?.classList.remove('fixed');
    // window.pageYOffset=100;
  };

  render() {
    return html`
      <div
        id="support-modal"
        class=" lg:mt-1  bg-opacity-25 bg-black absolute inset-0 h-screen w-screen hidden flex  justify-center items-center animated fadeIn faster"
      >
        <div
          class="support-myModal animate-fadeInUp shadow-inner bg-white flex flex-col relative inset-0 mx-auto rounded-xl p-8 w-11/12 lg:w-1/2 "
        >
          <div class="flex flex-row justify-between border-b-2 divide-gray-400">
            <h4 class="w-4/5 text-sm text-gray-500 font-semibold">
              If You Like What We Do Consider Supporting Us On These Platforms
            </h4>
            <button @click="${this.closeModal}" class="self-end mb-4">✕</button>
          </div>
          <div class="text-center">
            <h2 class="text-2xl font-semibold my-6 ">Support us through...</h2>
            <div
              class="grid sm:grid-cols-3 grid-cols-2 justify-between self-center gap-6 "
            >
              <paypal-icon
                class="cursor-pointer w-full  flex justify-center  self-center"
              ></paypal-icon>
              <revolut-icon
                class="cursor-pointer w-full  flex justify-center  self-center"
              ></revolut-icon>
              <div class="col-span-full grid-cols-1 sm:col-span-1">
                <patreon-icon
                  class="cursor-pointer w-full flex justify-center  self-center"
                ></patreon-icon>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('support-modal', SupportModal);
