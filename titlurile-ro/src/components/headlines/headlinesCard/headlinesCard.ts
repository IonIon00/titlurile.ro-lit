import { html, property } from 'lit-element';
import './modal.js';

import '../../icons.js';

import { YoloElement } from '../../../YoloElement.js';
import '../../popup/popup-component.js';

export class HeadlinesCard extends YoloElement {
  image: string;

  headlines: any;

  isLoading: boolean;

  description: string;

  index: number;

  url: string;

  width: number;

  isMobile: boolean = false;

  isVisible: boolean;

  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
      source: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.source = '';
    this.time = '';
    this.image = '';
    this.isLoading = true;
    this.headlines = [];
    this.description = '';
    this.index = 0;
    this.url = '';
    this.width = 0;
    this.isVisible = false;
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener('load', this.handleSize);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.removeEventListener('load', this.handleSize, true);
  }

  changeURL = (index: any) => {
    const theURL = window.location.pathname;
    return theURL.replace(`/product/${index}`, `/`);
  };

  @property({ type: String }) title: string;

  @property({ type: String }) source: string;

  @property({ type: String }) time: string;

  handleSize = () => {
    this.width = window.screen.width;

    this.isMobile = this.width < 1024;

    return this.width;
  };

  showModal = () => {
    const modal = this.querySelector('#popup-modal');

    modal?.classList.remove('hidden');

    const modalDialog = this.querySelector('.popup-myModal');

    // const body = document.querySelector('body');
    modalDialog?.classList.add('absolute');

    // if (!this.isMobile) {
    //   body?.classList.add('fixed');
    // } else {
    //   body?.classList.remove('fixed');
    // }

    modalDialog?.classList.add('top-3/5');
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.isVisible = true;
    console.log(this.isVisible);
  };

  render() {
    this.handleSize();
    return html` <div
        class="headline-card-class border rounded-3xl border-gray-400 border-opacity-50 mb-5"
      >
        <div class="flex flex-col lg:flex-row justify-between p-6 lg:pb-0">
          <div class="mb-8 mt-2 flex flex-col justify-start w-full">
            <a href="${this.url}">
              <h2 class="w-full lg:w-11/12 text-lg lg:text-xl font-semibold">
                ${this.title}
              </h2>
            </a>
            <p class="text-gray-400 text-sm lg:text-sm mt-1 mb-2">
              ${this.source} - ${this.time} hours ago
            </p>
            <div
              id="newsCompanies"
              class="flex flex-row mt-2 gap-2 overflow-x-scroll lg:overflow-x-hidden"
            >
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://i1.sndcdn.com/avatars-000261700769-0uhhy6-t500x500.jpg'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/6/60/Curierul_Național.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://i1.sndcdn.com/avatars-000261700769-0uhhy6-t500x500.jpg'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/6/60/Curierul_Național.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/f/fc/Logo-Libertatea.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
              <div class="border rounded-xl px-2 py-2">
                <headline-icon
                  .path=${'https://upload.wikimedia.org/wikipedia/commons/6/60/Curierul_Național.png'}
                ></headline-icon>
                <p class="mt-1 text-gray-400 lg:text-xs text-sm">
                  ${this.time}h ago
                </p>
              </div>
            </div>
            ${
              this.isMobile
                ? html`<a href="/news/${this.index}">
                    <button
                      class="hidden lg:block text-left mt-6 text-purple-700 font-bold tracking-wide news-button text-md"
                    >
                      VIEW FULL COVERAGE
                    </button>
                  </a>`
                : html` <button
                    @click="${this.showModal}"
                    class="hidden lg:block text-left mt-6 text-purple-700 font-bold tracking-wide news-button text-md"
                  >
                    VIEW FULL COVERAGE
                  </button>`
            }


          </div>
          <div class="flex flex-col justify-between">
            <img
              class="object-cover w-full lg:w-40 h-full lg:h-24"
              src="${this.image}"
              alt="news-headline"
            />
        ${
          this.isMobile
            ? html`<a href="/news/${this.index}"
                ><button
                  class="lg:hidden text-left mt-6 text-purple-700 font-bold tracking-wide news-button text-lg"
                  @click="${this.showModal}"
                >
                  VIEW FULL COVERAGE
                </button></a
              >`
            : html`<button
                class="lg:hidden text-left mt-6 text-purple-700 font-bold tracking-wide news-button text-lg"
                @click="${this.showModal}"
              >
                VIEW FULL COVERAGE
              </button>`
        }


          </div>
        </div>
      </div>
      <popup-component
      .time="${this.time}"
      .title="${this.title}"
      .image="${this.image}"
      .source="${this.source}"
      .description="${this.description}"
      .URL="${this.url}"
      .index="${this.index}"></popup-component>
  </div>`;
  }
}

customElements.define('headlines-card', HeadlinesCard);
