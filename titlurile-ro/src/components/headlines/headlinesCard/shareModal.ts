import { html } from 'lit-element';

import { YoloElement } from '../../../YoloElement.js';
import '../../icons/copy.js';
import '../../icons/facebook-icon.js';
import '../../icons/instagram-icon.js';
import '../../icons/linkedin-icon.js';

export class ShareModal extends YoloElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
  }

  connectedCallback() {
    super.connectedCallback();
  }

  closeModal = () => {
    const modal = this.querySelector('#share-modal');
    modal?.classList.add('hidden');
    const bodyElement = document.getElementsByTagName('BODY')[0];
    bodyElement.classList.remove('fixed');
    // window.pageYOffset=100;
    modal?.classList.add('fadeOut');
    modal?.classList.remove('fadeIn');
  };

  getURL = () => {
    document.execCommand('copy');
    alert('Copied the URL: ');
  };

  render() {
    return html`
      <div
        id="share-modal"
        class="   lg:mt-1  bg-opacity-25 bg-black absolute inset-0 h-screen w-screen hidden flex  justify-center items-center animated fadeIn faster"
      >
        <div
          class="share-myModal animate-fadeInUp shadow-inner bg-white flex flex-col relative inset-0 mx-auto rounded-xl p-8 w-11/12 lg:w-1/2 "
        >
          <div class="flex flex-row justify-between border-b-2 divide-gray-400">
            <h4 class="w-4/5 text-sm text-gray-500 font-semibold">
              ${this.title}
            </h4>
            <button @click="${this.closeModal}" class="self-end mb-4">✕</button>
          </div>
          <div class="text-center">
            <h2 class="text-2xl font-semibold my-6 ">Share to...</h2>
            <div
              class="grid sm:grid-cols-4 grid-cols-2 justify-between self-center gap-3 "
            >
              <button class="" @click="${this.getURL}">
                <copy-icon
                  class="cursor-pointer w-full flex justify-center  self-center"
                ></copy-icon>
              </button>
              <instagram-icon
                class="cursor-pointer w-full  self-center flex justify-center  "
              ></instagram-icon>
              <facebook-icon
                class="cursor-pointer w-full  self-center flex justify-center  "
              ></facebook-icon>
              <linkedin-icon
                class="cursor-pointer w-full  self-center flex justify-center  "
              ></linkedin-icon>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('share-modal', ShareModal);
