import { html, property } from 'lit-element';

import { YoloElement } from '../../../YoloElement.js';

import '../headlinesCard/headlinesCard.js';

// import { indexDB } from '../../algosearch.js';

export class HeadlinesList extends YoloElement {
  isLoading: boolean;

  headlines: any;

  static get properties() {
    return {
      headlines: { type: Array },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.image = '';
    this.headlines = [];
    this.isLoading = true;
  }

  @property({ type: String }) title: string;

  @property({ type: String }) image: string;

  timeDiffCalc = (dateNow: Date, datePast: Date) => {
    const dateNowMiliseconds = dateNow.getTime();

    const datePastMiliseconds = datePast.getTime();

    const difference = Math.abs(dateNowMiliseconds - datePastMiliseconds);

    const hours = Math.floor((difference / (1000 * 60 * 60)) % 24);

    return hours;
  };

  fetchNews = async () => {
    try {
      // const API_KEY = 'ae34a4b3475c4ad185afad31cdc4a4cf';

      // const baseURL = `https://newsapi.org/v2/top-headlines?country=ro&apiKey=${API_KEY}`;
      const baseURL = `http://localhost:3000/articles`;
      const response = await fetch(baseURL);
      const news = await response.json();

      // this.headlines = news.articles;
      this.headlines = news;
      console.log(this.headlines);
      this.isLoading = false;
    } catch (e) {
      console.log(e);
    }
  };

  connectedCallback() {
    super.connectedCallback();
    this.fetchNews();
  }

  render() {
    const result: any[] = this.headlines;

    // indexDB.saveObjects(result, { autoGenerateObjectIDIfNotExist: true });

    return html`
      ${result.map(
        (headline, index) => html`
          <headlines-card
            key=${index}
            .title="${headline.title}"
            .source="${headline.source.name}"
            .time="${this.timeDiffCalc(
              new Date(new Date().toISOString()),
              new Date(headline.publishedAt)
            ).toString()}"
            .image="${headline.urlToImage}"
            .description="${headline.description}"
            .index=${index}
            .url="${headline.url}"
          ></headlines-card>
        `
      )}
    `;
  }
}

customElements.define('headlines-list', HeadlinesList);
