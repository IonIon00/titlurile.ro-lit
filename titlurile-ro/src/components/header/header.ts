import { html, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';

import '../icons/hamburger-icon.js';

export class Header extends YoloElement {
  @property({ type: String }) title = 'Titlurile.ro';

  searchHandler = () => {};

  showMenu = () => {
    const leftPanel = document.querySelector('left-panel');
    leftPanel?.classList.toggle('-translate-x-full');
    leftPanel?.classList.toggle('hidden');
    leftPanel?.classList.toggle('col-end-3');
    leftPanel?.classList.toggle('col-span-full');
    document.querySelector('news-feed')?.classList.toggle('hidden');

    if (leftPanel?.classList.contains('hidden')) {
      this.isActive = false;
    } else {
      this.isActive = true;
    }
    if (this.isActive === false) {
      document.querySelector('news-feed')?.classList.remove('hidden');
    }

    if (this.isMobile) {
      leftPanel?.classList.remove('col-end-3');
      leftPanel?.classList.add('col-span-full');
    } else {
      leftPanel?.classList.add('col-end-3');
      leftPanel?.classList.remove('col-span-full');
      document.querySelector('news-feed')?.classList.remove('hidden');
    }

    console.log(this.isActive);
  };

  showSearch = () => {
    console.log(this);
    const searchBar = this.querySelector('.search-mobile');

    searchBar?.classList.toggle('hidden');
  };

  width: number;

  isMobile: boolean = false;

  isActive: boolean;

  constructor() {
    super();
    this.width = window.screen.width;
    this.isMobile = false;
    this.isActive = false;
  }

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener('resize', this.handleSize);
    this.handleSize();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.removeEventListener('resize', this.handleSize, true);
  }

  handleSize = () => {
    this.width = window.screen.width;

    this.isMobile = this.width < 1024;
    console.log(this.width, this.isMobile);
    const leftPanel = document.querySelector('left-panel');

    if (this.isMobile && this.isActive) {
      leftPanel?.classList.remove('col-end-3');
      leftPanel?.classList.add('col-span-full');
      document.querySelector('news-feed')?.classList.add('hidden');
      leftPanel?.classList.remove('hidden');
    } else {
      leftPanel?.classList.add('col-end-3');
      leftPanel?.classList.remove('col-span-full');
      document.querySelector('news-feed')?.classList.remove('hidden');
    }

    return this.width;
  };

  render() {
    console.log(this.width);
    return html`
      <div class="grid grid-cols-3 shadow-lg bg-white py-3  lg:py-0">
        <button class="col-start-1 lg:hidden ml-10 " @click="${this.showMenu}">
          <hamburger-icon></hamburger-icon>
        </button>
        <a href="/">
        <h3
          class="col-start-2 lg:col-start-1 py-1  sm:ml-16 lg:ml-14 lg:mt-4 text-lg lg:text-xl text-gray-700  font-bold uppercase"
          type="button"
        >
          ${this.title}
        </h3>
        </a>
        <div id="searchBar">
          <button class="" @click="${this.showSearch}">
            <svg
              class="absolute top-5 right-10 lg:hidden lg:mt-7 ml-20 lg:ml-2 xl:ml-2"
              width="20"
              height="20"
              viewBox="0 0 15 15"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M11.0362 9.42303C10.8921 9.27863 10.6965 9.19749 10.4925 9.19749H10.2751C10.2157 9.19749 10.1585 9.17442 10.1157 9.13315C10.0281 9.04865 10.0221 8.91079 10.0975 8.81526C10.7945 7.93267 11.2091 6.82013 11.2091 5.61625C11.2091 2.75922 8.8932 0.443359 6.03617 0.443359C3.17915 0.443359 0.863281 2.75922 0.863281 5.61625C0.863281 8.47328 3.17915 10.7891 6.03617 10.7891C7.24005 10.7891 8.3526 10.3746 9.23518 9.67761C9.33071 9.60217 9.46858 9.60818 9.55307 9.69581C9.59435 9.73861 9.61741 9.79575 9.61741 9.85521V10.0726C9.61741 10.2766 9.69855 10.4722 9.84295 10.6163L13.0031 13.7701C13.3309 14.0973 13.8619 14.0971 14.1894 13.7695C14.517 13.442 14.5173 12.911 14.19 12.5831L11.0362 9.42303ZM6.0354 9.19744C4.05379 9.19744 2.45417 7.59782 2.45417 5.61621C2.45417 3.63459 4.05379 2.03497 6.0354 2.03497C8.01702 2.03497 9.61664 3.63459 9.61664 5.61621C9.61664 7.59782 8.01702 9.19744 6.0354 9.19744Z"
                fill="#828282"
              />
            </svg>
          </button>
          <input
            class="invisible lg:visible col-start-3 lg:col-start-2 py-2 px-8 lg:mt-4 lg:mb-5 w-11/12 text-md lg:text-xl bg-gray-100 focus:outline-none rounded"
            type="search"
            placeholder=" Search"
            results="0"
            @keydown="${this.searchHandler}"
          />
        </div>
      </div>
      <input
        class="col-span-3 hidden xl:hidden font-roboto search-mobile shadow appearance-none border rounded w-full py-2 px-3 text-gray-900 font-normal leading-tight focus:outline-none focus:shadow-outline"
        id="username"
        type="search"
        placeholder="Search News..."
      />
      </div>
    `;
  }
}
customElements.define('header-component', Header);
