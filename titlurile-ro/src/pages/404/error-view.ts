import { html, css } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';
import '../../components/header/header.js';
import '../../components/left-panel/left-panel.js';
import '../../components/right-panel/right-panel.js';
import '../../components/headlines/headlinesCard/modal.js';

export class ErrorPage extends YoloElement {
  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      font-size: calc(10px + 2vmin);
      color: #1a2b42;
      max-width: 960px;
      margin: 0 auto;
      text-align: center;
      background-color: var(--titlurile-ro-background-color);
    }

    main {
      flex-grow: 1;
    }
  `;

  render() {
    return html`
      <main>
        <h1>404 Error. Page not Found</h1>
      </main>
    `;
  }
}

customElements.define('error-view', ErrorPage);
