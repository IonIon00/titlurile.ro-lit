import { html, css, property } from 'lit-element';

import { YoloElement } from '../../YoloElement.js';
import '../../components/header/header.js';
import '../../components/left-panel/left-panel.js';
import '../../components/right-panel/right-panel.js';
import '../../components/news-feed/news-feed.js';
import '../../components/headlines/headlinesCard/modal.js';

export class TitlurileRo extends YoloElement {
  image: string;

  headlines: any;

  isLoading: boolean;

  description: string;

  index: number;

  url: string;

  width: number;

  isMobile: boolean = false;

  source: string;

  time: string;

  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
      source: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.source = '';
    this.time = '';
    this.image = '';
    this.isLoading = true;
    this.headlines = [];
    this.description = '';
    this.index = 0;
    this.url = '';
    this.width = 0;
  }

  @property({ type: String }) title = 'My app';

  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      font-size: calc(10px + 2vmin);
      color: #1a2b42;
      max-width: 960px;
      margin: 0 auto;
      text-align: center;
      background-color: var(--titlurile-ro-background-color);
    }

    main {
      flex-grow: 1;
    }
  `;

  render() {
    return html`
      <main>
        <header-component></header-component>
        <div class="grid grid-cols-12">
          <left-panel
            class="col-start-1 col-end-3 hidden lg:block"
          ></left-panel>
          <news-feed
            class=" col-start-1 col-end-13 col-span-12 lg:col-start-3 lg:col-end-10 "
          ></news-feed>

          <right-panel
            class="mt-16 col-start-10 col-end-13 hidden lg:block"
          ></right-panel>
          <support-modal></support-modal>
        </div>
      </main>
    `;
  }
}

customElements.define('titlurile-ro', TitlurileRo);
