import { html, css, property } from 'lit-element';
import { YoloElement } from '../../YoloElement.js';
import '../../components/header/header.js';
import '../../components/left-panel/left-panel.js';
import '../../components/right-panel/right-panel.js';
import '../../components/headlines/headlinesCard/modal.js';

export class CardPage extends YoloElement {
  headlines: any;

  isLoading: boolean;

  index: number;

  width: number;

  isMobile: boolean = false;

  static get properties() {
    return {
      title: { type: String },
      time: { type: String },
      source: { type: String },
      headlines: { type: Array },
    };
  }

  constructor() {
    super();

    this.isLoading = true;
    this.headlines = [];
    this.index = 0;
    this.width = 0;
  }

  @property({ type: String }) title = 'My app';

  @property({ type: Object }) location = window.location;

  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      font-size: calc(10px + 2vmin);
      color: #1a2b42;
      max-width: 960px;
      margin: 0 auto;
      text-align: center;
      background-color: var(--titlurile-ro-background-color);
    }

    main {
      flex-grow: 1;
    }
  `;

  timeDiffCalc = (dateNow: Date, datePast: Date) => {
    const dateNowMiliseconds = dateNow.getTime();

    const datePastMiliseconds = datePast.getTime();

    const difference = Math.abs(dateNowMiliseconds - datePastMiliseconds);

    const hours = Math.floor((difference / (1000 * 60 * 60)) % 24);

    return hours;
  };

  firstUpdated() {
    this.fetchNews();
  }

  fetchNews = async () => {
    // const API_KEY = 'ae34a4b3475c4ad185afad31cdc4a4cf';

    // const baseURL = `https://newsapi.org/v2/top-headlines?country=ro&apiKey=${API_KEY}`;
    const baseURL = `http://localhost:3000/articles`;
    const response = await fetch(baseURL);
    const news = await response.json();

    // this.headlines = news.articles;
    this.headlines = news;
    this.isLoading = false;
    console.log(this.headlines);
  };

  render() {
    this.fetchNews();

    const regex = new RegExp(`\\d+`);

    const id = Number(window.location.pathname.match(regex));

    const result: any[] = this.headlines;

    console.log(result);

    console.log(id);

    return html`
      <main>
        <header-component></header-component>
        <div class="grid grid-cols-12">
          <left-panel
            class="col-start-1 col-end-3 hidden lg:block"
          ></left-panel>
          <div
            class="col-start-1 col-end-13 col-span-12 lg:col-start-3 lg:col-end-10"
          >
            <card-modal
              .title="${result[id].title}"
              .source="${result[id].source.name}"
              .image="${result[id].urlToImage}"
              .time="${this.timeDiffCalc(
                new Date(new Date().toISOString()),
                new Date(result[id].publishedAt)
              ).toString()}"
              key=${id}
              .description="${result[id].description}"
            ></card-modal>
          </div>

          <right-panel
            class="mt-10 col-start-10 col-end-13 hidden lg:block"
          ></right-panel>
          <support-modal></support-modal>
        </div>
      </main>
    `;
  }
}

customElements.define('card-page', CardPage);
