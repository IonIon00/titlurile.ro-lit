import { Router } from '@vaadin/router';

const initRouter = () => {
  const root = document.querySelector('#root');
  console.log(root);
  if (root) {
    const router = new Router(root);

    router.setRoutes([
      {
        path: '/',
        animate: true,
        component: 'titlurile-ro',
        action: async () => {
          await import('./pages/main-page/titlurile-ro.js');
        },
      },

      {
        path: '/news/:news',
        animate: true,
        component: 'card-page',
        action: async () => {
          await import('./pages/card-page/card-page.js');
        },
      },
      {
        path: '(.*)',
        animate: true,
        component: 'error-view',
        action: async () => {
          await import('./pages/404/error-view.js');
        },
      },
    ]);
  }
};

window.addEventListener('load', () => {
  initRouter();
  alert('Router called');
});
