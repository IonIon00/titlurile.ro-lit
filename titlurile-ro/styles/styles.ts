import { css } from 'lit-element';

export const styles = css`
  @tailwind base;
  @tailwind components;
  @tailwind utilities;
`;
