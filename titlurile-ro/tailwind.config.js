module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
    './src/**/*.ts',
    './dist/index.html',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      roboto: 'Roboto sans-serif',
      arial: 'Arial',
      new: 'Georama sans-serif',
    },
    fontSize: {
      xs: '.75rem',
      sm: '.875rem',
      tiny: '.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    colors: {
      'news-gray': '#333333',
      'news-black': 'rgba(0,0,0,87%)',
    },
    pseudo: {
      // defaults to {'before': 'before', 'after': 'after'}
      before: 'before',
      after: 'after',
      'not-first': 'not(:first-child)',
      'after-content': {
        content: '|',
      },
    },
  },
  variants: {
    extend: {},
    empty: ['before', 'after'],
  },
  plugins: [],
};
